# DBB Client JavaScript Library

# What is this?

DBB 서비스를 사용하기 위한 자바스크립트 라이브러리

# How do I use it?

## Building the repo

```sh
npm run build
```

## Type-checking the repo

```sh
npm run type-check
```

And to run in `--watch` mode:

```sh
npm run type-check:watch
```

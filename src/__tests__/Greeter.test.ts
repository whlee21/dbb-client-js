import { C, Greeter } from "../index";

test("My Greeter", () => {
    expect(Greeter("John")).toBe("Hello John");
});

test("My C", () => {
    let c = new C();
    expect(c.getX()).toBe(10);
});

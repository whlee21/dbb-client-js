export class C {
    private x = 10;
    constructor() {}

    getX = () => this.x;
    setX = (newVal: number) => {
        this.x = newVal;
    };
}

export let x = new C();
export let y = { ...{ some: "value" } };
export const Greeter = (name: string) => `Hello ${name}`;
